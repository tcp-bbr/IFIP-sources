#!/usr/bin/env python3


bdps = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0]

algos = ['bbr', 'cubic']

for algo in algos:
    for bdp in bdps:
        with open('configs/config_{}_{}_BDP.parm'.format(str(bdp), algo), 'w') as f:
            f.write('''
Information: {bdp}_{algo}_BDP
Bandwidth: 10Mbit
Burst Buffer: 1600b
Buffer Latency: {lat}ms
Git Commit: 131d8b5587988a391cafd7af4fd10bb90574e5e7

Commands:
host, {algo}, 50ms, 0.0, 120.0
host, {algo}, 50ms, 0.0, 120.0
host, {algo}, 50ms, 0.0, 120.0
host, {algo}, 50ms, 0.0, 120.0
host, {algo}, 50ms, 0.0, 120.0'''.format(algo=algo, lat=50 * bdp, bdp=bdp))
