#!/usr/bin/env python3

import numpy as np

intervals = np.arange(0.1, 22.0, 0.1)

for inter in intervals:
    with open('configs/config_{}.parm'.format(str(inter)), 'w') as f:
        f.write('''
Information: {inter}
Bandwidth: 10Mbit
Burst Buffer: 1600b
Buffer Latency: 500ms
Git Commit: c19044a14f66337f783c03cde06336a9827dd8f6

Commands:
bbr, 40ms, 0.0, 120.0
bbr, 40ms, {inter}, {a}
bbr, 40ms, {inter}, {b}
bbr, 40ms, {inter}, {c}
bbr, 40ms, {inter}, {d}'''.format(inter=inter, a=120-inter, b=120-2*inter, c=120-3*inter, d=120-4*inter))
