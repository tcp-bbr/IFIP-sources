#!/usr/bin/env python3

import numpy as np

bbr = [1, 2, 3, 5, 10]
cubic = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for b in bbr:
    for c in cubic:
        with open('configs/config_bbr_{}_cubic_{}.parm'.format(str(b), str(c)), 'w') as f:
            f.write('''
Information: {c}
Bandwidth: 10Mbit
Burst Buffer: 1600b
Buffer Latency: 125.0ms
Git Commit: af46eef6ec9f39378c93090e546f2f750dec0b5a

Commands:
'''.format(c=c))
            for num in range(0, b):
                f.write("bbr, 50ms, 0.0, 180.0\n")
            f.write("cubic, 50ms, 1.0, 179.0\n")
            for num in range(0, c-1):
                f.write("cubic, 50ms, 0.0, 179.0\n")
            break
