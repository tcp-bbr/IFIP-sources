#!/usr/bin/env python3

rtts = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 120,
        140, 160, 180, 200]

for rtt in rtts:
    with open('configs/config_rtt_{}.parm'.format(str(rtt).zfill(3)), 'w') as f:
        f.write('''
Information: {rtt}
Bandwidth: 10Mbit
Burst Buffer: 1600b
Buffer Latency: 500ms
Git Commit: a05cbca17872f39ff8b767e1b300e653fe8984ce

Commands:
bbr, {rtt}ms, 0.0, 120.0
bbr, {rtt}ms, 0.0, 120.0
bbr, {rtt}ms, 0.0, 120.0
bbr, {rtt}ms, 0.0, 120.0
bbr, {rtt}ms, 0.0, 120.0'''.format(rtt=rtt))
